
const yargs = require('yargs');

const apiProvider = require('./provider/api_provider');
const forecastProvider = require('./provider/forecast_api');

const argv = yargs
    .option({
        provice_id: {
            demand: true,
            alias: 'pid',
            describe: 'Pencarian berdasar Provinsi ID',
            string: true
        }
    }).help().alias('help', 'h').argv;

apiProvider.GetProvince(argv.provice_id, (errorMessage, results) => {
    if (errorMessage) {
        console.log("Error : ", errorMessage);
    } else {
        console.log(JSON.stringify(results, undefined, 2));
    }
});



forecastProvider.GetWeather(-6.224880, 106.805270, (errorMessage, weatherResults) => {
    if (errorMessage) {
        console.log(errorMessage);
    } else {
        console.log(JSON.stringify(weatherResults, undefined, 2));
    }
})