
const yargs = require('yargs');

const apiProvider = require('./provider/api_provider');
const forecastProvider = require('./provider/forecast_api');

const argv = yargs
    .option({
        provice_id: {
            demand: true,
            alias: 'pid',
            describe: 'Pencarian berdasar Provinsi ID',
            string: true
        }
    }).help().alias('help', 'h').argv;