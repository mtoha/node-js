const request = require('request');

var getProvinceByID = (province_id) => {
    return new Promise((resolve, reject) => {
        var encodedProvinceID = encodeURIComponent(province_id);

        request({
            url: `https://api.rajaongkir.com/starter/province?key=e7c1b5f5b1025ff7a0e7364c3c0664f4&id=${encodedProvinceID}`,
            json: true
        }, (error, response, body) => {
            if (error) {
                reject('Error akses raja api');
            } else if (body.rajaongkir.status.description !== "OK") {
                reject('API RajaOngkir gagal');
            } else {
                resolve({
                    provinsi: body.rajaongkir.results.province
                });
            }
        });
    })
}

getProvinceByID(1).then((province) => {
    console.log(JSON.stringify(province, undefined, 2));
}, (errorMessage) => {
    console.log(errorMessage);
})