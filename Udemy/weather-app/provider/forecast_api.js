const request = require('request');

var GetWeather = (lat, long, callback) => {
    request({
        url: `https://api.darksky.net/forecast/dee76152fb97b7f084b3c30587e75b4b/${lat},${long}`,
        json: true
    }, (error, response, body) => {
        if (error) {
            callback('Error calling forecast.io');
        } else if (response.statusCode === 400) {
            callback('Unable fetch weather');
        } else if (response.statusCode === 200) {
            callback(undefined, {
                temperature: body.currently.temperature
                ,apparentTemperature :body.currently.apparentTemperature
            });
        }
    })
}

module.exports.GetWeather = GetWeather;