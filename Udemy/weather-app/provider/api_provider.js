const request = require('request');

var GetProvince = (provice_id, callback) => {

    var encodedProvinceID = encodeURIComponent(provice_id);

    request({
        url: `https://api.rajaongkir.com/starter/province?key=e7c1b5f5b1025ff7a0e7364c3c0664f4&id=${encodedProvinceID}`,
        json: true
    }, (error, response, body) => {
        if (error) {
            callback('Error akses raja api');
        } else if (body.rajaongkir.status.description !== "OK") {
            callback('API RajaOngkir gagal');
        } else {
            //console.log(JSON.stringify(body, undefined, 2));
            callback(undefined, { 'provinsi': body.rajaongkir.results.province });
            //console.log(response);
        }
    });
}


module.exports.GetProvince = GetProvince;