
module.exports.age = 25;

// module.exports.addNote = () => {
//     console.log('Add Note');
//     return 'New Note';
// }
var fetchNotes = () => {

    try {
        var notesString = fs.readFileSync('notes-data.json');
        return JSON.parse(notesString);
    } catch (e) {
        return [];
    }
}

var saveNotes = (notes) => {
    fs.writeFileSync('notes-data.json', JSON.stringify(notes));

}
const fs = require('fs');
var addNote = (title, body) => {
    var notes = fetchNotes();
    var note = {
        title,
        body
    }

    var duplicateNotes = notes.filter((note) =>
        note.title === title
    );

    if (duplicateNotes.length === 0) {
        notes.push(note);
        saveNotes(notes);
        return note;
    }
}
var add = (a, b) => {
    return a + b;
}

var getAll = () => {
    return fetchNotes();
}

var getNote = (title) => {
    console.log('Getting note : ', title);
    var notes = fetchNotes();
    var filteredNote = notes.filter((note) => note.title === title)
    return filteredNote[0];
}

var logNote = (note) => {
    console.log("title : ", note.title);
    console.log("Body of note : ", note.body);
}

var removeNote = (title) => {
    console.log('Removing Note : ', title);
    var notes = fetchNotes();
    var filteredNote = notes.filter((note) => note.title !== title);

    saveNotes(filteredNote);
    return notes.length !== filteredNote.length;
}
module.exports = {
    addNote
    , add
    , getAll
    , getNote
    , removeNote
    , logNote
}