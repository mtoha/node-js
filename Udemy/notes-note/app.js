console.log('Starting App...');

const fs = require('fs');
const _ = require('lodash');
const os = require('os');
const yargs = require('yargs')

const notes = require('./notes.js')
var user = os.userInfo();

console.log(user);

//fs.appendFile("greetings.txt", `Hello ${user.username} You are ${notes.age}.`);

// var res = notes.addNote();
// console.log(res);

// console.log('Result : ' + notes.add(8, -2));

// console.log(_.isString(true))
// console.log(_.isString('Toha'))

// var filteredArray = _.uniq(['Mike', 1, 'Toha', 1, 2, 3, 4]);
// console.log(filteredArray);

// console.log(process.argv);

var command = process.argv[2];
console.log('Command : ', command);

var titleOptions = {
    describe: 'Title of Note',
    demand: true,
    alias: 't'
}
var bodyOptions = {
    describe: 'Body of note',
    demand: true,
    alias: 'b'
}
const argv = yargs
    .command('add', 'Add a new Note', {
        title: titleOptions,
        body: bodyOptions
    }).command('list', 'list all notes')
    .command('read', 'Read a note', {
        title: titleOptions
    }).command('remove', 'Remove a note by title', {
        title: titleOptions
    }).help().argv;

if (command == 'add') {
    console.log('Adding New Note')
    var note = notes.addNote(argv.title, argv.body);
    if (note) {
        notes.logNote(note);
    } else {
        console.log("Note title maybe taken");
    }
}
else if (command == 'list') {
    console.log('Listing all notes')
    var allNotes = notes.getAll();
    console.log(`Printing ${allNotes.length} note(s)`);
    allNotes.forEach((note) => notes.logNote(note));
} else if (command == 'read') {
    var note = notes.getNote(argv.title);
    if (note) {
        console.log("Note found");
        notes.logNote(note);
    } else {
        console.log("Note not found");

    }
} else if (command == 'remove') {
    var noteRemoved = notes.removeNote(argv.title);
    var message = noteRemoved ? "Note removed" : "Remove Note Failed"
    console.log(message)
} else
    console.log('Wrong command')

console.log('Yargs', argv);