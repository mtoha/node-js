const Joi = require('joi');
const express = require('express');
const app = express();
const path = require('path');
const mongoose = require('mongoose');
var bodyParser = require('body-parser');
const expressValidator = require('express-validator');
const flash = require('connect-flash');
const session = require('express-session');


//parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
//parse application/json
app.use(bodyParser.json())

//set Public Folder
app.use(express.static(path.join(__dirname, 'public')));

//Express Session MiddleWare
app.use(session({
    secret: 'keyboard cat',
    resave: true,
    saveUninitialized: true,
    cookie: { secure: true }
}));

//Express Messages MiddleWare
app.use(require('connect-flash'));
app.use(function (req, res, next) {
    res.locals.messages = require('express-messages')(req, res);
    next();
})

//Express Validator Middleware
app.use(expressValidator({
    errorFormatter: function (param, msg, value) {
        var namespace = param.split('.')
            , root = namespace.shift()
            , formParam = root;

        while (namespace.length) {
            formParam += '[' + namespace.shift() + ']';
        }
        return {
            param : formParam,
            msg : msg,
            value : value
        }
    }
}))

//mongo connection
mongoose.connect('mongodb://localhost/nodekb');
let db = mongoose.connection;

//Check connection
db.once('open', function () {
    console.log('Connected to Mongo DB');
})

//Check DB Errors
db.on('error', function (err) {
    console.log(err);
});

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

let Article = require('./models/article');

app.get('/articles', function (req, res) {
    // let articles = [{
    //     id: 1,
    //     title: 'Article One',
    //     author: 'Mtoha 2013',
    //     body: 'This is body of article one'
    // }, {
    //     id: 2,
    //     title: 'Article Two',
    //     author: 'Mtoha 2013',
    //     body: 'This is body of article Two'
    // }, {
    //     id: 3,
    //     title: 'Article Three',
    //     author: 'Mtoha 2013',
    //     body: 'This is body of article Three'
    // }]
    let articles = Article.find({}, function (err, articles) {
        if (err)
            console.log(err)
        else {
            res.render('index', {
                title: 'Hello MTH',
                articles: articles
            });
        }
    })
})

app.get('/article/:id', function (req, res) {
    Article.findById(req.params.id, function (err, article) {
        res.render('article', {
            article: article
        });
    });
});

app.get('/article/add', function (req, res) {
    res.render('add', {
        title: 'Add an Article'
    });
})


app.get('/article/edit/:id', function (req, res) {
    Article.findById(req.params.id, function (err, article) {
        res.render('article_edit', {
            title: "Edit Article",
            article: article
        });
    });
})


app.post('/article/edit/:id', function (req, res) {
    let article = {};
    article.title = req.body.title;
    article.author = req.body.author;
    article.body = req.body.body;
    console.log(article);

    let query = { _id: req.params.id }
    Article.update(query, article, function (err) {
        if (err) {
            console.log(err);
            return;
        } else {
            res.redirect('/articles')
        }
    })
});


app.post('/article/add', function (req, res) {
    let article = new Article();
    article.title = req.body.title;
    article.author = req.body.author;
    article.body = req.body.body;
    console.log(article);
    article.save(function (err) {
        if (err) {
            console.log(err);
            return;
        } else {
            res.redirect('/articles')
        }
    })
});

app.delete('/article/:id', function (req, res) {
    let query = { _id: req.params.id }

    Article.remove(query, function (err) {
        if (err) {
            console.log(err);
        }
    })
    res.send('Success');
});

app.use(express.json());

const courses = [{
    id: 1, name: 'Math'
},
{
    id: 2, name: 'Physics'
},
{
    id: 3, name: 'Biology'
}]

app.get('/', (request, response) => {
    response.send('Hello world...');
});
app.get('/api/courses', (req, res) => {
    res.send(courses);
});

app.get('/api/courses/:id', (req, res) => {
    const course = courses.find(c => c.id === parseInt(req.params.id));
    if (!course) return res.status(404).send('The course with given ID was not found');
    res.send(course);
});
app.get('/api/courses/:year/:month', (req, res) => {
    res.send(req.query);
    res.send(req.params);

});

app.post('/api/courses', (req, res) => {
    const { error } = validateCourse(req.body);
    if (error)
        return res.status(400).send(error.details[0].message);


    const course = {
        id: courses.length + 1,
        name: req.body.name
    }
    courses.push(course);
    res.send(courses);
});
app.put('/api/courses/:id', (req, res) => {
    const course = courses.find(c => c.id === parseInt(req.params.id));
    if (!course) return res.status(404).send('The course with given ID was not found');
    const { error } = validateCourse(req.body);
    if (error) {
        res.status(400).send(error.details[0].message);
        return;
    }

    course.name = req.body.name;
    res.send(course);
});

app.delete('/api/courses/:id', (req, res) => {
    const course = courses.find(c => c.id === parseInt(req.params.id));
    if (!course) return res.status(404).send('The course with given ID was not found');

    const index = courses.indexOf(course);
    courses.splice(index, 1);

    res.send(course);
});
const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening port ${port}`));

function validateCourse(course) {
    const Schema = {
        name: Joi.string().min(3).required()
    }
    return Joi.validate(course, Schema)
}