var module1 = require('./ModulMahasiswa.js');

module1.print();

module1.name = "Fidyar Pardiman";
module1.age = 30;

module1.print();

console.log("----------------");
require("./Person.js"); // Panggil skill dan set language mjd .NET
console.log("----------------");
require("./PersonFidyar.js"); // Skill otomatis .NET krn Instance Skills adalah objct shared dan hidup


console.log("----------------");
require("./ObjectFactory/PersonFidyarFactory.js"); // Skill otomatis .NET krn Instance Skills adalah objct shared dan hidup
console.log("----------------");
require("./ObjectFactory/PersonFactory.js"); // Panggil skill dan set language mjd .NET
