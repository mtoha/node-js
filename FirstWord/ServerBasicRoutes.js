var http = require('http');
var router = require('routes')();
var url = require('url');

var port = 8888;

router.addRoute('/', function (req, res) {
    res.writeHead(200, {"Content-Type": "text/plain"});
    res.end("Index Page");
});

router.addRoute('/profile/:nama/:kota', function (req, res) {
    res.writeHead(200, {"Content-Type": "text/plain"});
    res.end("Profile Page >> " + this.params.nama + " dari " + this.params.kota);
});

router.addRoute('/contact', function (req, res) {
    res.writeHead(200, {"Content-Type": "text/plain"});
    res.end("Contact Page");
});

http.createServer(function (req, res) {
    var path = url.parse(req.url).pathname;
    var match = router.match(path);
    if (match) {
        match.fn(req, res);
    } else {
        res.writeHead(404, {"Content-Type": "text/plain"});
        res.end("Page Not Found!");
    }
    // if(req.url != "/favicon.ico"){
    //     res.writeHead(200,{"Content-Type": "text/plain"});
    //     res.write("Hello from Node JS Server");
    //     res.write("You request url : " + req.url);
    //     res.end();
    //
    // }
}).listen(port);

console.log("Server running on port :" + port);