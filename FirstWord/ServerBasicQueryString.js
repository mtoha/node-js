var http = require('http');
var url = require('url');
var fs = require('fs');
var QString = require('querystring');
var port = 8888;
http.createServer(function (req, res) {
    var access = url.parse(req.url);
    var data = QString.parse(access.query);
    console.log(data);
    if (access.pathname != "/favicon.ico") {
        if (access.pathname == "/") {
            kode = 200;
            file = "index.html";
        } else if (access.pathname == "/contact") {
            //Contact
            kode = 200;
            file = "Contact.html";
        } else {
            //Not Found
            kode = 404;
            file = "NotFound.html";
        }
        res.writeHead(kode, {"Content-Type": "text/html"});
        fs.createReadStream('./html/' + file).pipe(res);
    }
}).listen(port);

console.log("Server running on port :" + port);