var qString = require('querystring');
var http = require('http');
var router = require('routes')();
var url = require('url');
var mysql = require('mysql');
var view = require('swig');
var connection = mysql.createConnection({
    host: "localhost",
    port: 3306,
    database: "NodeJS",
    user: "root",
    password: ""
});
var port = 8888;

router.addRoute('/', function (req, res) {
    connection.query("select * from mahasiswa", function (err, rows, field) {
        if (err) throw err;

        var html = view.compileFile('./template/index.html')({
            title: "Data Mahasiswa",
            data: rows
        });
        res.writeHead(200, { "Content-Type": "text/html" });
        res.end(html);
    });

});

router.addRoute('/insert', function (req, res) {
    console.log(req.method.toUpperCase());
    if (req.method.toUpperCase() == "POST") {
        var data_post = "";
        req.on('data', function (chuncks) {
            data_post += chuncks;
        });

        req.on('end', function () {
            data_post = qString.parse(data_post);
            console.log(data_post);
            connection.query("insert into mahasiswa set ? ", data_post, function (err, field) {
                if (err) throw err;

                res.writeHead(302, { "Location": "/" })
                res.end();
            });
        });
    } else {
        var html = view.compileFile('./template/form.html')();
        res.writeHead(200, { "Content-Type": "text/html" });
        res.end(html);
    }
});

router.addRoute('/update/:id', function (req, res) {
    connection.query("select * from mahasiswa where ?",
        { no_induk: this.params.id },
        function (err, rows, field) {
            if (err) throw err;

            if (rows.length) {
                var data = rows[0];
                if (req.method.toUpperCase() == "POST") {
                    var data_post = "";
                    req.on('data', function (chuncks) {
                        data_post += chuncks;
                    });
                    req.on('end', function () {
                        data_post = qString.parse(data_post);
                        console.log(data_post);
                        connection.query("update mahasiswa set ? where ?", [
                            data_post,
                            {
                                no_induk: data.no_induk
                            }], function (err, field) {
                                if (err) throw err;

                                res.writeHead(302, { "Location": "/" })
                                res.end();
                            });
                    });
                } else {
                    var html = view.compileFile('./template/form_update.html')({
                        data: data
                    });
                    res.writeHead(200, { "Content-Type": "text/html" });
                    console.log('Get End')
                    res.end(html);
                }
            } else {
                var html = view.compileFile('./template/404.html')();
                res.writeHead(404, { "Content-Type": "text/html" });
                res.end(html);
            }
        });
    console.log(req.method.toUpperCase());
});


router.addRoute('/delete/:id', function (req, res) {
    connection.query("delete from mahasiswa where ?", {
        no_induk: this.params.id
    }, function (err, field) {
        if (err) throw err;

        res.writeHead(302, { "Location": "/" })
        res.end();
    });

});


router.addRoute('/profile/:nama/:kota', function (req, res) {
    res.writeHead(200, { "Content-Type": "text/plain" });
    res.end("Profile Page >> " + this.params.nama + " dari " + this.params.kota);
});

router.addRoute('/contact', function (req, res) {
    res.writeHead(200, { "Content-Type": "text/plain" });
    res.end("Contact Page");
});

http.createServer(function (req, res) {
    var path = url.parse(req.url).pathname;
    var match = router.match(path);
    if (match) {
        match.fn(req, res);
    } else {
        res.writeHead(404, { "Content-Type": "text/plain" });
        res.end("Page Not Found!");
    }
    // if(req.url != "/favicon.ico"){
    //     res.writeHead(200,{"Content-Type": "text/plain"});
    //     res.write("Hello from Node JS Server");
    //     res.write("You request url : " + req.url);
    //     res.end();
    //
    // }
}).listen(port);

console.log("Server running on port :" + port);