var http = require('http');
var fs = require('fs');


var port = 8888;
http.createServer(function (req, res) {
    var kode = 0;
    var file = "";
    if (req.url == "/") {
        kode = 200;
        file = "index.html";
    } else if (req.url == "/contact") {
        //Contact
        kode = 200;
        file = "Contact.html";
    } else {
        //Not Found
        kode = 404;
        file = "NotFound.html";
    }
    res.writeHead(kode, {"Content-Type": "text/html"});
    fs.createReadStream('./html/' + file).pipe(res);
}).listen(port);

console.log("Server running on port :" + port);