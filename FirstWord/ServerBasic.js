var http = require('http');

var port = 8888;
http.createServer(function (req, res) {
    if(req.url != "/favicon.ico"){
        res.writeHead(200,{"Content-Type": "text/plain"});
        res.write("Hello from Node JS Server");
        res.write("You request url : " + req.url);
        res.end();

    }
}).listen(port);

console.log("Server running on port :" + port);