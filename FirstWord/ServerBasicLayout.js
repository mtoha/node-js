var http = require('http');
var router = require('routes')();
var url = require('url');
var view = require('swig');
var port = 8888;

router.addRoute('/', function (req, res) {
    var html = view.compileFile('./html/index.html')({
       title : "Index Page (c) M Toha"
    });
    res.writeHead(200, {"Content-Type": "text/html"});
    res.end(html);
});

router.addRoute('/profile/:nama/:kota', function (req, res) {
    var html = view.compileFile('./html/profile.html')({
        title : "Profile Page (c) M Toha"
    });
    res.writeHead(200, {"Content-Type": "text/html"});
    res.end(html);
});

router.addRoute('/contact', function (req, res) {
    var html = view.compileFile('./html/contact.html')({
        title : "Contact Page (c) M Toha"
    });
    res.writeHead(200, {"Content-Type": "text/html"});
    res.end(html);
});

router.addRoute('/form', function (req, res) {
    var html = view.compileFile('./html/form.html')({
        title : "Form Page (c) M Toha"
    });
    res.writeHead(200, {"Content-Type": "text/html"});
    res.end(html);
});


http.createServer(function (req, res) {
    var path = url.parse(req.url).pathname;
    var match = router.match(path);
    if (match) {
        match.fn(req, res);
    } else {
        res.writeHead(404, {"Content-Type": "text/plain"});
        res.end("Page Not Found!");
    }
    // if(req.url != "/favicon.ico"){
    //     res.writeHead(200,{"Content-Type": "text/plain"});
    //     res.write("Hello from Node JS Server");
    //     res.write("You request url : " + req.url);
    //     res.end();
    //
    // }
}).listen(port);

console.log("Server running on port :" + port);